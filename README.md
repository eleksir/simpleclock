# Simple Clock

## Just clock that shows time, accurately.

### Component base is very simple:

* Arduino NANO v3 chinese replica (on Atmega-168 chip)
* 7 segment 4 digit LED indicator with common anode, FYQ-5641BD-21 (red LEDs)
* 8 resistors that limiting current on LED segments, 330 Ohms, 0.25 watts
* RTC Module GEEGROW with DS1307 chip (very basic, only RTC chip, quartz resonator, button battery, no additional memory chip)
* 5x7 cm single side prototyping board
* usb phone charger as power supply
* 2 push buttons
* 1 on/off switch
* 1 photoresistor
* 1 resistor to make secund shoulder of photosensitive "bridge", 10 kOhms, 0.25 watts
* Gainta G521G enclosure (92x66.5x28мм)
* 5m power cord

### Optionally you may need:

* Capasitor with well capacity if your charger has small capasitor on output
* Fuse, to protect your device
* DC/DC transformer if you have power supply that gives more than 5 volts or less than 4.5 volts (this will give additional surge protection if your power supply dies, this can happen after 3-5 years of uninterrupted work)

### Additional Notes:

My RTC module is not very accurate due to bad quartz chrystal calibration, looks like it is not exactly 32768 Hz and clock runs faster than it should be, giving about 5-8 seconds runaway per day. So i have to implement time correction in software (temperature changes during day/month/year is not very big - about 10 degrees Celsius, so we can use software compensation without thermal compensation).
